v0.1.0, 12.05.2013 -- Initial release
v0.1.1, 12.05.2013 -- Moved typ into main module
v0.1.2, 12.05.2013 -- Corrected README.txt
v0.1.3, 12.05.2013 -- Corrected README.txt reST syntax
v0.1.4, 12.05.2013 -- Edited README.txt reST syntax. Will add more readme content on further release.